Source: pyftdi
Maintainer: Anton Gladky <gladk@debian.org>
Section: libs
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-dev,
               python3-serial,
               python3-pytest,
               python3-setuptools,
               python3-ruamel.yaml,
               sphinx,
               python3-sphinx-autodoc-typehints,
               python3-sphinx-rtd-theme,
               python3-usb
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/debian/pyftdi
Vcs-Git: https://salsa.debian.org/debian/pyftdi.git
Homepage: https://github.com/rm-hull/ftdi
Testsuite: autopkgtest-pkg-python

Package: python3-ftdi
Architecture: any
Section: python
Depends: ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Description: user-space driver for popular FTDI devices
 Suported FTDI devices include:
  UART and GPIO bridges
      FT232R (single port, 3Mbps)
      FT230X/FT231X/FT234X/ (single port, 3Mbps)
  UART, GPIO and multi-serial protocols (SPI, I2C, JTAG) bridges
      FT2232C/D (dual port, clock up to 6 MHz)
      FT232H (single port, clock up to 30 MHz)
      FT2232H (dual port, clock up to 30 MHz)
      FT4232H (quad port, clock up to 30 MHz)

Package: python3-ftdi-doc
Architecture: all
Section: doc
Depends: ${sphinxdoc:Depends},
         ${misc:Depends}
Description: user-space driver for popular FTDI devices. Documentation
 Suported FTDI devices include:
  UART and GPIO bridges
      FT232R (single port, 3Mbps)
      FT230X/FT231X/FT234X/ (single port, 3Mbps)
  UART, GPIO and multi-serial protocols (SPI, I2C, JTAG) bridges
      FT2232C/D (dual port, clock up to 6 MHz)
      FT232H (single port, clock up to 30 MHz)
      FT2232H (dual port, clock up to 30 MHz)
      FT4232H (quad port, clock up to 30 MHz)
  Package contains documentation
